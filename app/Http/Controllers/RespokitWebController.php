<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class RespokitWebController extends Controller {

    public function index(){

        return view('dashboard');
    }
    
        public function userprofile(){

            return view('user');
        }

        public function usermaps(){

            return view('maps');
        }
        public function usertodashboard(){

            return view('dashboard');
        }
        public function usertotable(){

            return view('table');
        }
        public function tabletodashboard(){

            return view('dashboard');
        }
        public function tabletouser(){

            return view('user');
        }
        public function tabletotable(){

            return view('table');
        }
        public function login(){

            return view('login');
        }
        public function register(){
            return view('register');
        }
        public function form(){
            return view('form');
        }
        }

    // public function "pangalan sa function" = para sa function
    // return view('file') = para sa pag tawag sa file na gamiton
    // return vuew('folder.file') = if naa sa folder ang file
 
    



