<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Residents List</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <script src="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('css/light-bootstrap-dashboard.css?v=2.0.1')}}" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{URL::asset('css/demo.css')}}" rel="stylesheet" />
</head>

<body >
    <div class="wrapper">
        <div class="sidebar" data-image="{{URL::asset('css/sidebar-5.jpg')}}">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="http://www.creative-tim.com" class="simple-text">
                        Respokit
                    </a>
                </div>
                <ul class="nav">
                    <li>
                        <a class="nav-link" href="{{('dashboard')}}">
                        
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{('user')}}">
                    
                            <p>User Profile</p>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{('table')}}">
                   
                            <p>Residents List</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="./typography.html">
                         
                        <p>(another options pa dre)</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class=" container-fluid  ">
                    <p>Dashboard</p>
                    
                    
                </div>
            </nav>
            
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

<div class="jumbotron">
<div class="row w-100">
        <div class="col-md-3" style="
    padding-left: 15px;
    left: 15px;
">
            <div class="card border-info mx-sm-1 p-3">
                <div class="card border-info shadow text-info p-3 my-card" ><span class="fa fa-car" aria-hidden="true"></span></div>
                <div class="text-info text-center mt-3"><h4>Children</h4></div>
                <div class="text-info text-center mt-3"><h6>male</h6></div>
                <div class="text-info text-center mt-3"><h6>64</h6></div>
                <div class="text-info text-center mt-3"><h6>famale</h6></div>
                <div class="text-info text-center mt-3"><h6>100</h6></div>
                <div class="text-info text-center mt-2"><h1>164</h1></div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-success mx-sm-1 p-3">
                <div class="card border-success shadow text-success p-3 my-card"><span class="fa fa-eye" aria-hidden="true"></span></div>
                <div class="text-success text-center mt-3"><h4>Adult</h4></div>
                <div class="text-info text-center mt-3"><h6>male</h6></div>
                <div class="text-info text-center mt-3"><h6>112</h6></div>
                <div class="text-info text-center mt-3"><h6>famale</h6></div>
                <div class="text-info text-center mt-3"><h6>150</h6></div>
                <div class="text-success text-center mt-2"><h1>262</h1></div>
            </div>
        </div>

        <div class="col-md-3"style="
    padding-right: 0px;
">
            <div class="card border-warning mx-sm-1 p-3">
                <div class="card border-warning shadow text-warning p-3 my-card" ><span class="fa fa-inbox" aria-hidden="true"></span></div>
                <div class="text-warning text-center mt-3"><h4>Elderly</h4></div>
                <div class="text-info text-center mt-3"><h6>male</h6></div>
                <div class="text-info text-center mt-3"><h6>35</h6></div>
                <div class="text-info text-center mt-3"><h6>famale</h6></div>
                <div class="text-info text-center mt-3"><h6>45</h6></div>
                <div class="text-warning text-center mt-2"><h1>80</h1></div>
                </div>              
                 </div>
        <div class="col-md-3">
            <div class="card border-dark mx-sm-1 p-3">
                <div class="card border-dark shadow text-dark p-3 my-card"><span class="fa fa-eye" aria-hidden="true"></span></div>
                <div class="text-dark text-center mt-3"><h4>Demographic Profile</h4></div>
                <div class="text-info text-center mt-3"><h6>male</h6></div>
                <div class="text-info text-center mt-3"><h6>211</h6></div>
                <div class="text-info text-center mt-3"><h6>famale</h6></div>
                <div class="text-info text-center mt-3"><h6>295</h6></div>
                <div class="text-dark text-center mt-2"><h1>506</h1></div>
            </div>
        </div>
        </div>
        </div>
        </div>




</body>
<!--   Core JS Files   -->
<script src="{{URL::asset('js/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{URL::asset('js/bootstrap-switch.js')}}"></script>
<!--  Chartist Plugin  -->
<script src="{{URL::asset('js/chartist.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{URL::asset('js/bootstrap-notify.js')}}"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="{{URL::asset('js/light-bootstrap-dashboard.js?v=2.0.1')}}" type="text/javascript"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="{{URL::asset('js/demo.js')}}"></script>


</html>
<style>
.my-card
{
    position:absolute;
    left:40%;
    top:-20px;
    border-radius:50%;
}

div.container {
        width: 80%;
    }
</style>
