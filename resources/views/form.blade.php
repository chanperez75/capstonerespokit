<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  
  <title>Form</title>

  
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="css/style2.css">

  
</head>

<body>

  
<div class="container">
  <form>
      <div class="row">
          <h4>Barangay</h4>
          <div class="input-group input-group-icon">
            <input type="text" placeholder="Name of Barangay"/>
            <div class="input-icon"><i class="fa fa-institution"></i></div>
          </div>
          <div class="input-group input-group-icon">
            <input type="text" placeholder="Purok"/>
            <div class="input-icon"><i class="fa fa-map-marker"></i></div>
          </div>
        </div>
    <div class="row">
      <h4>Full name</h4>
      <div class="input-group input-group-icon">
        <input type="text" placeholder="Last name"/>
        <div class="input-icon"><i class="fa fa-user"></i></div>
      </div>
      <div class="input-group input-group-icon">
        <input type="text" placeholder="First name"/>
        <div class="input-icon"><i class="fa fa-user"></i></div>
      </div>
      <div class="input-group input-group-icon">
        <input type="text" placeholder="Middle name"/>
        <div class="input-icon"><i class="fa fa-user"></i></div>
      </div>
    </div>
    <div class="row">
      <div class="col-half">
        <h4>Date of Birth</h4>
        <div class="input-group">
          <div class="col-third">
            <input type="text" placeholder="DD"/>
          </div>
          <div class="col-third">
            <input type="text" placeholder="MM"/>
          </div>
          <div class="col-third">
            <input type="text" placeholder="YYYY"/>
          </div>
        </div>
      </div>
      <div class="col-half">
        <h4>Gender</h4>
        <div class="input-group">
          <input type="radio" name="gender" value="male" id="gender-male"/>
          <label for="gender-male">Male</label>
          <input type="radio" name="gender" value="female" id="gender-female"/>
          <label for="gender-female">Female</label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="input-group input-group-icon">
        <input type="text" placeholder="Place of Birth"/>
        <div class="input-icon"><i class="fa fa-globe"></i></div>
      </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3"><label for="selectSm" class=" form-control-label"><h4 style="padding-left: 190px;">Blood type</h4></label></div>
        <div class="col-8 col-md-9">
          <select name="selectSm" id="SelectLm" class="form-control-sm form-control chan">
            <option value="0">Please select</option>
            <option value="1">A+</option>
            <option value="2">A-</option>
            <option value="3">B+</option>
            <option value="4">B-</option>
            <option value="5">AB+</option>
            <option value="6">AB-</option>
            <option value="7">O+</option>
            <option value="8">O-</option>
          </select>
        </div>
      </div>

      <div class="row form-group">
          <div class="col col-md-3"><label for="selectSm" class=" form-control-label"><h4 style="padding-left: 190px;">Civil status</h4></label></div>
          <div class="col-12 col-md-9">
            <select name="selectSm" id="SelectLm" class="form-control-sm form-control chan">
              <option value="0">Please select</option>
              <option value="1">Annulled</option>
              <option value="2">Married</option>
              <option value="3">Widowd/er</option>
              <option value="4">Separated</option>
              <option value="5">Single</option>
            </select>
          </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3"><label for="selectSm" class=" form-control-label"><h4 style="padding-left: 190px;">Tenurial status</h4></label></div>
            <div class="col-12 col-md-9">
              <select name="selectSm" id="SelectLm" class="form-control-sm form-control chan">
                <option value="0">Please select</option>
                <option value="1">Owner</option>
                <option value="2">Caretaker</option>
                <option value="3">Living with relatives</option>
                <option value="4">Renter</option>
                <option value="5">Leaving w/parents</option>
              </select>
            </div>
          </div>


          <div class="row"><br> <br>
              <div class="input-group input-group-icon"> 
                <input type="text" placeholder="Home address"/>
                <div class="input-icon"><i class="fa fa-globe"></i></div>
              </div>
            </div>
            <div class="row">
              <div class="input-group input-group-icon"> 
                <input type="text" placeholder="Mobile"/>
                <div class="input-icon"><i class="fa fa-mobile-phone"></i></div>
              </div>
            </div>
            <div class="row">
              <div class="input-group input-group-icon"> 
                <input type="text" placeholder="Email address"/>
                <div class="input-icon"><i class="fa fa-desktop"></i></div>
              </div>
            </div>
            <div class="row">
                <div class="input-group input-group-icon"> 
                  <input type="text" placeholder="Religion"/>
                  <div class="input-icon"><i class=""></i></div>
                </div>
              </div>
  

              <div class="row form-group">
                  <div class="col col-md-3"><label for="selectSm" class=" form-control-label"><h4 style="padding-left: 190px;">Tribe</h4></label></div>
                  <div class="col-12 col-md-9">
                    <select name="selectSm" id="SelectLm" class="form-control-sm form-control chan">
                      <option value="0">Please select</option>
                      <option value="1">Aeta</option>
                      <option value="2">Aklanon</option>
                      <option value="3">Antiqueño</option>
                      <option value="4">Badjao</option>
                      <option value="5">Banwaon</option>
                      <option value="5">Bagobo</option>
                      <option value="5">Bilaan</option>
                      <option value="5">Boholano</option>
                      <option value="5">Bukidnon</option>
                      <option value="5">Cebuano</option>
                      <option value="5">Chavacanon</option>
                      <option value="5">Dabaweño</option>
                      <option value="5">Higaonon</option>
                      <option value="5">Igorot</option>
                      <option value="5">Ilocano</option>
                      <option value="5">Ilongo</option>
                      <option value="5">Kalagan</option>
                      <option value="5">Kalayan</option>
                      <option value="5">Kalibay</option>
                      <option value="5">Kaulo</option>
                      <option value="5">Leyteño</option>
                      <option value="5">Manguindanao</option>
                      <option value="5">Mamanwa</option>
                      <option value="5">Mandaya</option>
                      <option value="5">Manguangan</option>
                      <option value="5">Manobo</option>
                      <option value="5">Mansaka</option>
                      <option value="5">Maranao</option>
                      <option value="5">Mausaka</option>
                      <option value="5">Pampagueño</option>
                      <option value="5">Sangil</option>
                      <option value="5">Sequihornon</option>
                      <option value="5">subanen</option>
                      <option value="5">Surigaonon</option>
                      <option value="5">Tagakaulo</option>
                      <option value="5">Tagalog</option>
                      <option value="5">Talaandig</option>
                      <option value="5">Tausog</option>
                      <option value="5">Tiruray</option>
                      <option value="5">T'boli</option>
                      <option value="5">Ubo</option>
                      <option value="5">Waray</option>
                      <option value="5">Others</option>

                    </select>
                  </div>
                </div>
                <div class="row">
                    <div class="col-half">
                      <h4>Precint No:</h4>
                      <div class="input-group">
                        <div class="col-fifth">
                          <input type="text" placeholder="Precint No."/>
                        </div>
                      </div>
                    </div>
                    <div class="col-half">
                      <h4>Comelec Registered voter</h4>
                      <div class="input-group">
                        <input type="radio" name="voter" value="vyes" id="voter-yes"/>
                        <label for="voter-yes">Yes</label>
                        <input type="radio" name="voter" value="vno" id="voter-no"/>
                        <label for="voter-no">No</label>
                      </div>
                    </div>
                  </div>
                
                  
              <div class="row form-group">
                  <div class="col col-md-3"><label for="selectSm" class=" form-control-label"><h4 style="padding-left: 150px;">Educational attainment</h4></label></div>
                  <div class="col-12 col-md-9">
                    <select name="selectSm" id="SelectLm" class="form-control-sm form-control chan">
                      <option value="0">Please select</option>
                      <option value="1">Elementary Education</option>
                      <option value="2">Vocational</option>
                      <option value="3">Out of School</option>
                      <option value="4">High School (Jr. High)</option>
                      <option value="4">High School (Sr. High)</option>
                      <option value="5">College level</option>
                      <option value="5">College Graduate</option>
                    </select>
                  </div>
                </div>

                <div class="row form-group">
                    <div class="col-half">
                        <h4>Skilled worker</h4>
                        <div class="input-group">
                          <input type="radio" name="sworker" value="syes" id="skilled-yes"/>
                          <label for="skilled-yes">Yes</label>
                          <input type="radio" name="sworker" value="sno" id="skilled-no"/>
                          <label for="skilled-no">No</label>
                        </div>
                        
                      </div>
                    </div>

                    <div class="row form-group">
                      <!-- Material checked -->
<div class="form-check">
  <h4>Skilled type</h4>
    <input type="checkbox" class="form-check-input" id="scarpentry">
    <label class="form-check-label" for="scarpentry">Carpentry</label>
    <input type="checkbox" class="form-check-input" id="sencoder">
    <label class="form-check-label" for="sencoder">Encoder</label>
    <input type="checkbox" class="form-check-input" id="scomtechnician">
    <label class="form-check-label" for="scomtechnician">Computer Technicial</label>
    <input type="checkbox" class="form-check-input" id="scomprogrammer">
    <label class="form-check-label" for="scomprogrammer">Computer programmer</label>
    <input type="checkbox" class="form-check-input" id="scooking">
    <label class="form-check-label" for="scooking">Cooking</label>
    <input type="checkbox" class="form-check-input" id="sdressmaker">
    <label class="form-check-label" for="sdressmaker">Dress maker</label>
    <input type="checkbox" class="form-check-input" id="sdriving">
    <label class="form-check-label" for="sdriving">Driving</label>
    <input type="checkbox" class="form-check-input" id="sfarming">
    <label class="form-check-label" for="sfarming">Farming</label>
    <input type="checkbox" class="form-check-input" id="sgraphicartist">
    <label class="form-check-label" for="sgraphicartist">Graphic artist</label>
    <input type="checkbox" class="form-check-input" id="smason">
    <label class="form-check-label" for="smason" style="left: 220px;bottom: 180px;">Mason</label>
    <input type="checkbox" class="form-check-input" id="sentertainer">
    <label class="form-check-label" for="sentertainer" style="left: 220px;bottom: 180px;">Entertainer</label>
    <input type="checkbox" class="form-check-input" id="smechanic">
    <label class="form-check-label" for="smechanic" style="left: 220px;bottom: 180px;">Mechanic</label>
    <input type="checkbox" class="form-check-input" id="spainter">
    <label class="form-check-label" for="spainter" style="left: 220px;bottom: 180px;">Painter</label>
    <input type="checkbox" class="form-check-input" id="swatchrepair">
    <label class="form-check-label" for="swatchrepair" style="left: 220px;bottom: 180px;">Watch repair</label>
    <input type="checkbox" class="form-check-input" id="snone">
    <label class="form-check-label" for="snone" style="left: 220px;bottom: 180px;">None</label>  
  </div>
    

  <div class="row form-group">
      <div class="form-check">
          <h4>Employment status</h4>
            <input type="checkbox" class="form-check-input" id="semployedgov">
            <label class="form-check-label" for="semployedgov">Employed (Government)</label>
            <input type="checkbox" class="form-check-input" id="semployedprvt">
            <label class="form-check-label" for="semployedprvt">Employed (Private)</label>
            <input type="checkbox" class="form-check-input" id="snoneunemployed">
            <label class="form-check-label" for="snoneunemployed">None/Unemployed</label>
            <input type="checkbox" class="form-check-input" id="sretired">
            <label class="form-check-label" for="sretired">Retired</label>
            <input type="checkbox" class="form-check-input" id="sselfemployed">
            <label class="form-check-label" for="sselfemployed">Self Employed</label>
            <input type="checkbox" class="form-check-input" id="sstudent">
            <label class="form-check-label" for="sstudent">Student</label>
            <div class="col-half">
                <h4>Occupation</h4>
                <div class="input-group">
                  <div class="col-fifth">
                    <input type="text" placeholder="Occupation"/>
                  </div>
                </div>
  </div>
                <div class="row">
                    
                    <div class="col-half">
                      <h4>PWD?</h4>
                      <div class="input-group">
                        <input type="radio" name="pwd" value="pwdyes" id="pwd-yes"/>
                        <label for="pwd-yes">Yes</label>
                        <input type="radio" name="pwd" value="pwdno" id="pwd-no"/>
                        <label for="pwd-no">No</label>
                      </div>
                    </div>
                  </div>
                  <div class="row form-group chan3">
                      <div class="form-check">
                          <h4>PWD type</h4>
                            <input type="checkbox" class="form-check-input" id="pwdautistic">
                            <label class="form-check-label" for="pwdautistic">Autistic</label>
                            <input type="checkbox" class="form-check-input" id="pwdcelebral">
                            <label class="form-check-label" for="pwdcelebral">Celebral palsy</label>
                            <input type="checkbox" class="form-check-input" id="pwdcleftpalate">
                            <label class="form-check-label" for="pwdcleftpalate">Cleft palate</label>
                            <input type="checkbox" class="form-check-input" id="pwdcrosseyed">
                            <label class="form-check-label" for="pwdcrosseyed">Cross Eyed</label>
                            <input type="checkbox" class="form-check-input" id="pwddeaf">
                            <label class="form-check-label" for="pwddeaf">Deaf</label>
                            <input type="checkbox" class="form-check-input" id="pwddeformity">
                            <label class="form-check-label" for="pwddeformity">Deformity</label>
                            <input type="checkbox" class="form-check-input" id="pwddsyndrome">
                            <label class="form-check-label" for="pwddsyndrome">Down Syndrome</label>
                            <input type="checkbox" class="form-check-input" id="pwdfracture">
                            <label class="form-check-label" for="pwdfracture">Fracture legs/arms</label>
                            <input type="checkbox" class="form-check-input" id="pwdhuncback">
                            <label class="form-check-label" for="pwdhuncback">Huncback</label>
                            <input type="checkbox" class="form-check-input" id="pwdhydrocephalus">
                            <label class="form-check-label" for="pwdhydrocephalus">Hydrocephalos</label>
                            <input type="checkbox" class="form-check-input" id="pwdinabilitytowalk">
                            <label class="form-check-label" for="pwdinabilitytowalk">In ability to walk alone</label>
                            <input type="checkbox" class="form-check-input" id="pwdmentalinpairment">
                            <label class="form-check-label" for="pwdmentalinpairment" style="left: 220px;bottom: 180px;">Mental impairment</label>
                            <input type="checkbox" class="form-check-input" id="pwdmissingarmlegs">
                            <label class="form-check-label" for="pwdmissingarmlegs" style="left: 220px;bottom: 180px;">Missing one or both arms/legs</label>
                            <input type="checkbox" class="form-check-input" id="pwdmuteanddeaf">
                            <label class="form-check-label" for="pwdmuteanddeaf" style="left: 220px;bottom: 180px;">Mute and Deaf</label>
                            <input type="checkbox" class="form-check-input" id="pwdparazedfromneckdown">
                            <label class="form-check-label" for="pwdparazedfromneckdown" style="left: 220px;bottom: 180px;">Paralyzed from neck down</label>
                            <input type="checkbox" class="form-check-input" id="pwdparalyzedlegs">
                            <label class="form-check-label" for="pwdparalyzedlegs" style="left: 220px;bottom: 180px;">Paralyzed legs</label>
                            <input type="checkbox" class="form-check-input" id="pwdparalyzedarms">
                            <label class="form-check-label" for="pwdparalyzedarms" style="left: 220px;bottom: 180px;">Paralyzed arms</label>
                            <input type="checkbox" class="form-check-input" id="pwdpolio">
                            <label class="form-check-label" for="pwdpolio" style="left: 220px;bottom: 180px;">Polio</label>
                            <input type="checkbox" class="form-check-input" id="pwdspeechdisorder">
                            <label class="form-check-label" for="pwdspeechdisorder" style="left: 220px;bottom: 180px;">Speech Disorder</label>
                            <input type="checkbox" class="form-check-input" id="pwdblindofbotheyes">
                            <label class="form-check-label" for="pwdblindofbotheyes" style="left: 220px;bottom: 180px;">Total blind of both eye</label>
                            <input type="checkbox" class="form-check-input" id="pwdblindnessofoneeye">
                            <label class="form-check-label" for="pwdblindnessofoneeye" style="left: 220px;bottom: 180px;">Total blindness of one eye</label>                           
                           
                                  </div>
                                </div>
                                </div>

                                <div class="row">
                                  <h4>SPOUSE INFORMATION (if Married)</h4>
                                  <div class="input-group input-group-icon"> 
                                    <input type="text" placeholder="Fullname"/>
                                    <div class="input-icon"><i class="fa fa-user"></i></div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="input-group input-group-icon"> 
                                    <input type="text" placeholder="Contact no."/>
                                    <div class="input-icon"><i class="fa fa-mobile-phone"></i></div>
                                  </div>
                                </div>
                                <div class="row">
                                  <h4>Parents information</h4>
                                  <div class="input-group input-group-icon"> 
                                    <input type="text" placeholder="Mothers name"/>
                                    <div class="input-icon"><i class="fa fa-user"></i></div>
                                  </div>
                                </div>
                                <div class="row">
                                    <div class="input-group input-group-icon"> 
                                      <input type="text" placeholder="Fathers name"/>
                                      <div class="input-icon"><i class="fa fa-user"></i></div>
                                    </div>
                                  </div>
                                  <div class="row">
                                      <div class="input-group input-group-icon"> 
                                        <input type="text" placeholder="Contact no."/>
                                        <div class="input-icon"><i class="fa fa-mobile-phone"></i></div>
                                      </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-group input-group-icon"> 
                                          <input type="text" placeholder="Address"/>
                                          <div class="input-icon"><i class="fa fa-globe"></i></div>
                                        </div>
                                      </div>
                        
                      
                                      <div class="row">
                                        <h4>Declared Annual income</h4>
                                          <div class="input-group input-group-icon"> 
                                            <input type="number" placeholder=""/>
                                          </div>
                                        </div>
                          
                                        <div class="row">
                    
                                            <div class="col-half">
                                              <h4 style="margin-left: 185px;width: 250px;">Philhealth member</h4>
                                              <div class="input-group chan4">
                                                <input type="radio" name="philhealth" value="philhealthyes" id="philhealth-yes"/>
                                                <label for="philhealth-yes">Yes</label>
                                                <input type="radio" name="philhealth" value="philhealthno" id="philhealth-no"/>
                                                <label for="philhealth-no">No</label>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="row">
                    
                                              <div class="col-half">
                                                <h4 style="margin-left: 210px;width: 250px;">4P'S member</h4>
                                                <div class="input-group chan4">
                                                  <input type="radio" name="4ps" value="4psyes" id="4ps-yes"/>
                                                  <label for="4ps-yes">Yes</label>
                                                  <input type="radio" name="4ps" value="4psno" id="4ps-no"/>
                                                  <label for="4ps-no">No</label>
                                                </div>
                                              </div>
                                            </div>
  
                                            <div class="row">
                                                <h4>Date stayed in this brgy.</h4>
                                                  <div class="input-group input-group-icon"> 
                                                    <input type="text" placeholder="Since:"/>
                                                  </div>
                                                </div>


                                                <div class="container-fluid  margin chan5">
	<button  href="https://www.behance.net/sakiran" target="_blank" class="themeBtn">Submit</button>
</div>

  </form>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  

    <script  src="js/index2.js"></script>




</body>

</html>
<style>
.chan {
    margin-left: 120px;
}
.chan1{
  color: #060303;
}
.chan3{
    height: 320px;
}
.chan4{
    margin-left: 125px;
    width: 250px;

}
.margin{
    margin-top:20px;    
    margin-bottom:20px;
}

.themeBtn {
    background: #ff5c00;
    color: #ffffff !important;
    display: inline-block;
    font-size: 15px;
    font-weight: 500;
    height: 50px;
    line-height: 0.8;
    padding: 18px 30px;
    text-transform: capitalize;
    border-radius: 1px;
    letter-spacing: 0.5px;
	border:0px !important;
	cursor:pointer;
	border-radius:100px;

}
a:hover{
    color: #ffffff;
    text-decoration:none;
}
.themeBtn:hover {
    background: rgb(255, 92, 0);
    color: #ffffff;
    box-shadow: 0 10px 25px -2px rgba(255, 92, 0, 0.6);
}
.themeBtn2 {
    background: #7600ff;
    color: #ffffff !important;
    display: inline-block;
    font-size: 15px;
    font-weight: 500;
    height: 50px;
    line-height: 0.8;
    padding: 18px 30px;
    text-transform: capitalize;
    border-radius: 1px;
    letter-spacing: 0.5px;
	border:0px !important;
	cursor:pointer;
	border-radius:100px;

}
.themeBtn2:hover {
    background: rgb(118, 0, 255);
    color: #ffffff;
    box-shadow: 0 10px 25px -2px rgba(118, 0, 255, 0.6);
}
.themeBtn3 {
    background: #ff2e4d;
    color: #ffffff !important;
    display: inline-block;
    font-size: 15px;
    font-weight: 500;
    height: 50px;
    line-height: 0.8;
    padding: 18px 30px;
    text-transform: capitalize;
    border-radius: 1px;
    letter-spacing: 0.5px;
	border:0px !important;
	cursor:pointer;
	border-radius:100px;

}
.themeBtn3:hover {
    background: rgb(255, 46, 77);
    color: #ffffff;
    box-shadow: 0 10px 25px -2px rgba(255, 46, 77, 0.6);
}
.themeBtn4 {
    background: #006eff;
    color: #ffffff !important;
    display: inline-block;
    font-size: 15px;
    font-weight: 500;
    height: 50px;
    line-height: 0.8;
    padding: 18px 30px;
    text-transform: capitalize;
    border-radius: 1px;
    letter-spacing: 0.5px;
	border:0px !important;
	cursor:pointer;
	border-radius:100px;

}
.themeBtn4:hover {
    background: rgb(0, 110, 255);
    color: #ffffff;
    box-shadow: 0 10px 25px -2px rgba(0, 110, 255, 0.6);
}
.chan5 {
    margin-left: 190px;

}
</style>