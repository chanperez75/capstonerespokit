<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('login', 'RespokitWebController@login');
Route::get('user','RespokitWebController@userprofile');
Route::get('dashboard','RespokitWebController@usertodashboard');
Route::get('table','RespokitWebController@usertotable');
Route::get('dashboard','RespokitWebController@tabletodashboard');
Route::get('user','RespokitWebController@tabletouser');
Route::get('register', 'RespokitWebController@register');
Route::get('form','RespokitWebController@form');

// Route::get = para sa linking (example = Route::get('pangalan sa route', 'Pangalan sa controller @ unsa na function iyang gamiton function'))

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


